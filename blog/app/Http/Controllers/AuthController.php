<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }
    public function welcome(Request $request)
    {
        return view('welcome');
    }
    public function welcome_nama(Request $request)
    {
        //dd($request->all());
        $nama = $request["nama_depan"];
        $namabelakang = $request["nama_belakang"];
        return view('welcome', compact('nama','namabelakang'));
    }
    //
}
