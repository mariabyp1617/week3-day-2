<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label>First Name:</label><br>
        <input type="text" name="nama_depan"/> <br><br>
        <label>Last Name:</label><br>
        <input type="text" name="nama_belakang"/> <br><br>
        <label>Gender:</label><br>
        <input type="radio" name="jk"/> Male <br>
        <input type="radio" name="jk"/> Female <br>
        <input type="radio" name="jk"/> Other <br><br>
        <label>Nationality:</label><br>
        <select>
            <option>Indonesian</option>
            <option>US</option>
            <option>China</option>
        </select><br><br>
        <label>Language spoken:</label><br>
        <input type="checkbox"/> Bahasa Indonesia <br>
        <input type="checkbox"/> English <br>
        <input type="checkbox"/> Other <br><br>
        <label>Bio:</label><br>
        <textarea rows="7" cols="25"></textarea><br />
        <a href="/welcome/{nama}"><button >Sign Up</button> </a>
    </form>
    

</body>
</html>